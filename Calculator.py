# Python 3.7.3
# Calculator for simple math expressions
# *parenthesis, decimals, negative numbers, 4 basic operations
# *x(x+y) not yet implemented


class Check:
    def __init__(self, string):
        self.string = string.replace(' ', '')
        self.index = 0

    def current_character(self):
        return self.string[self.index:self.index + 1]

    def has_next(self):
        return self.index < len(self.string)

    def expression(self):
        value = self.low_precedence()
        return value

    def low_precedence(self):
        values = [self.high_precedence()]
        while True:
            char = self.current_character()
            if char == '+':
                self.index += 1
                values.append(self.high_precedence())
            elif char == '-':
                self.index += 1
                values.append(-1 * self.high_precedence())
            else:
                break
        return sum(values)

    def high_precedence(self):
        values = [self.parenthesis()]
        while True:
            char = self.current_character()
            if char == '*':
                self.index += 1
                values.append(self.parenthesis())
            elif char == '/':
                self.index += 1
                divisor = self.parenthesis()
                if divisor == 0:
                    print('Division by 0 is undefined! Result ignoring division is:')
                    continue
                else:
                    values.append(1.0 / divisor)
            else:
                break
        value = 1.0
        for each in values:
            value *= each
        return value

    def parenthesis(self):
        char = self.current_character()
        if char == '(':
            self.index += 1
            value = self.expression()
            if self.current_character() != ')':
                print('No closing parenthesis!')
                print('Result ignoring parenthesis is:')
            self.index += 1
            return value
        else:
            return self.negative()

    def negative(self):

        char = self.current_character()
        if char == '-':
            self.index += 1
            return -1 * self.parenthesis()
        elif char == '+':
            self.index += 1
            return self.parenthesis()
        else:
            return self.number()

    def number(self):

        value = ''
        decimal_found = False
        while self.has_next():
            char = self.current_character()
            if char == '.':
                if decimal_found:
                    print(' Invalid input!', '\n', 'Unexpected . at index:', self.index)
                    evaluate()
                decimal_found = True
                value += '.'
            elif char.isdigit():
                value += char
            else:
                break
            self.index += 1

        return float(value)


def evaluate():
    while True:
        string = input('Please enter expression: ')
        if len(string) == 0:
            print('Empty input!', '\n')
        elif 'c' in string or 'C' in string:
            print('Clear!', '\n')
            pass
        elif 'x' in string or 'X' in string:
            print('Thank you for using this ultra fast, modern calculator! ')
            exit()
        else:
            for i in string:
                if i not in ' +-*/.()0123456789':
                    print('Invalid input!', '\n')
                    break
            else:
                try:
                    c = Check(string)
                    result = c.expression()
                except ValueError:
                    print('Invalid input!', '\n')
                    continue
                if int(result) == result:
                    print('=', int(result), '\n')
                else:
                    print('=', result, '\n')


evaluate()
