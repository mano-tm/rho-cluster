# String formatting
#string = "The Inquisitor must meet Varric on top of Skyhold's battlements to be introduced."
string = "The Inquisitor must meet Varric on top of Skyhold's battlements to be quisitor."
patches = [[5, 14, "Conquistador"], [26, 31, "King"], [43, 49, "Palace"]]

print
for start, end, patch in patches: string = string[:start+1] + patch + string[end:]
print(string)

# Variation with index number
#for i in patches: string = string[:i[0]+1] + i[2] + string[i[1]:]

# Variations with replace function(The output is different for the second string):
#string1 = "The Inquisitor must meet Varric on top of Skyhold's battlements to be quisitor."

#for i in patches: string = string.replace(string[i[0]+1:i[1]], i[2])
#for start, end, patch in patches: string = string.replace(string[start+1:end], patch)
