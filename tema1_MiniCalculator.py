# Mini Calculator using 0-9 digits with cancel entry(CE) and data verification.
print("----- Mini Calculator -----", "\n")
print("This calculator supports the following mathematical operations:")
print("+ to add numbers")
print("- to subtract numbers")                      # Menu
print("* to multiply numbers")
print("/ to divide numbers")
print("Type C to delete last entry, Type X to exit", '\n')

permitted_op = ['+', '-', '*', '/']
operation = 0

while operation == 0:
    while operation not in permitted_op:            # Check if the user chooses a valid option from the menu.
        operation = input('Select operation from %s: ' % permitted_op)
        operation = operation.strip()

        if operation.lower() == 'c':                # Check if the user wants to delete last entry(no entry yet).
            print('Nothing to delete yet', '\n')
            operation = 0
        elif operation.lower() == 'x':              # Check if the user wants to exit.
            break
        elif operation not in permitted_op:         # Check if the user enters a permitted operation.
            print('Invalid Operation', '\n')
        else:
            pass

    if operation == '+':                            # Adding numbers
        a = 0
        while a == 0:
            a = input('Enter the first number: ')
            a = a.strip()
            if a.lower() == 'c':                    # Check if the user wants to delete last entry(operation)
                operation = 0
            elif a.lower() == 'x':
                break
            elif a.isdigit():                       # Check if first number is natural
                a = int(a)
                b = 0
                while b == 0:
                    b = input('Enter the second number: ')
                    b = b.strip()
                    if b.lower() == 'c':            # Check if the user wants to delete last entry(first number, a)
                        a = 0
                    elif b.lower() == 'x':
                        break
                    elif b.isdigit():               # Check if second number is natural
                        b = int(b)
                        print('The result for your expression is:')
                        print(a, operation, b, '=', a+b, '\n')

                        r = input('Perform another operation? (Y/N) ')
                        r = r.strip()
                        if r.lower() == 'c':        # Check if the user wants to delete last entry(second number, b)
                            b = 0
                            a = 1
                        elif r.lower() == 'x':
                            break
                        elif r.lower() == 'y':      # Check if the user wants to perform another operation
                            operation = 0
                            a = 1
                            b = 1
                        else:
                            break
                    else:
                        print('The number must contain only digits from 0 to 9', '\n')
                        b = 0
            else:
                print('The number must contain only digits from 0 to 9', '\n')
                a = 0

    elif operation == '-':                          # Subtracting numbers
        a = 0
        while a == 0:
            a = input('Enter the first number: ')
            a = a.strip()
            if a.lower() == 'c':
                operation = 0
            elif a.lower() == 'x':
                break
            elif a.isdigit():
                a = int(a)
                b = 0
                while b == 0:
                    b = input('Enter the second number: ')
                    b = b.strip()
                    if b.lower() == 'c':
                        a = 0
                    elif b.lower() == 'x':
                        break
                    elif b.isdigit():
                        b = int(b)
                        print('The result for your expression is:')
                        print(a, operation, b, '=', a-b, '\n')

                        r = input('Perform another operation? (Y/N) ')
                        r = r.strip()
                        if r.lower() == 'c':
                            b = 0
                            a = 1
                        elif r.lower() == 'x':
                            break
                        elif r.lower() == 'y':
                            operation = 0
                            a = 1
                            b = 1
                        else:
                            break
                    else:
                        print('The number must contain only digits from 0 to 9', '\n')
                        b = 0
            else:
                print('The number must contain only digits from 0 to 9', '\n')
                a = 0

    elif operation == '*':                          # Multiplying numbers
        a = 0
        while a == 0:
            a = input('Enter the first number: ')
            a = a.strip()
            if a.lower() == 'c':
                operation = 0
            elif a.lower() == 'x':
                break
            elif a.isdigit():
                a = int(a)
                b = 0
                while b == 0:
                    b = input('Enter the second number: ')
                    b = b.strip()
                    if b.lower() == 'c':
                        a = 0
                    elif b.lower() == 'x':
                        break
                    elif b.isdigit():
                        b = int(b)
                        print('The result for your expression is:')
                        print(a, operation, b, '=', a*b, '\n')

                        r = input('Perform another operation? (Y/N) ')
                        r = r.strip()
                        if r.lower() == 'c':
                            b = 0
                            a = 1
                        elif r.lower() == 'x':
                            break
                        elif r.lower() == 'y':
                            operation = 0
                            a = 1
                            b = 1
                        else:
                            break
                    else:
                        print('The number must contain only digits from 0 to 9', '\n')
                        b = 0
            else:
                print('The number must contain only digits from 0 to 9', '\n')
                a = 0

    elif operation == '/':                          # Dividing numbers
        a = 0
        while a == 0:
            a = input('Enter the first number: ')
            a = a.strip()
            if a.lower() == 'c':
                operation = 0
            elif a.lower() == 'x':
                break
            elif a.isdigit():
                a = int(a)
                b = 0
                while b == 0:
                    b = input('Enter the second number: ')
                    b = b.strip()
                    if b.lower() == 'c':
                        a = 0
                    elif b.lower() == 'x':
                        break
                    elif b.isdigit():
                        b = int(b)
                        if b == 0:                   # Check data, Division by 0
                            print('Division by 0 in undefined', '\n')
                        else:
                            print('The result for your expression is:')
                            print(a, operation, b, '=', a/b, '\n')

                            r = input('Perform another operation? (Y/N) ')
                            r = r.strip()
                            if r.lower() == 'c':
                                b = 0
                                a = 1
                            elif r.lower() == 'x':
                                break
                            elif r.lower() == 'y':
                                operation = 0
                                a = 1
                                b = 1
                            else:
                                break
                    else:
                        print('The number must contain only digits from 0 to 9', '\n')
                        b = 0
            else:
                print('The number must contain only digits from 0 to 9', '\n')
                a = 0
    else:
        pass
else:
    print()
    print('Thank you for using my Mini Calculator')
